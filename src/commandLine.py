import sys
from urllib.parse import urlparse

def config():
    maxdepth = 3

    if len(sys.argv) < 2:
        print("Error: no URL supplied")
        sys.exit()
    if not absolute():
        print("Error: Invalid URL supplied.")
        print("Please supply an absolute URL to this program")
        sys.exit()
    if len(sys.argv) == 3:
        maxdepth = int(sys.argv[2])

    url = sys.argv[1]

    return maxdepth, url

def absolute():
    absURL = True

    #if sys.argv[1].endswith('/'):
    #    sys.argv[1] = sys.argv[1][:-1]

    temp = urlparse(sys.argv[1])
    dummy = temp.scheme + '://' + temp.netloc + temp.path

    if sys.argv[1] != dummy:
        absURL = False

    return absURL