import sys
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup


def crawl(url, maxdepth):
    visited = set()
    visited.add("http://www.usu.edu/lyric")
    visited.add("https://fafsa.gov")
    visited.add("https://fsaid.ed.gov/npas/index.htm")
    visited.add("https://studentaid.ed.gov/sa")
    visited.add("http://www.fafsa.ed.gov")
    visited.add("http://fafsa.ed.gov")
    visited.add("http://studentloans.gov")
    visited.add("https://studentclearinghouse.org")
    visited.add("https://huntsman.usu.edu/acct/files/SOA_Newsletter_2018.pdf")
    visited.add("http://www.usu.edu/radio")
    visited.add("http://boxelder4h.usu.edu")

    print(f"Crawling from {url} to a maximum depth of {maxdepth} links")
    crawling(url,0,maxdepth,visited)

def crawling(URL,depth,maxdepth,visited):
    #Base Cases
    if depth == maxdepth:
        depth -= 1
        return
    if URL in visited:
        depth -= 1
        return
    if '://' not in URL:
        depth -= 1
        return

    if URL.endswith('/'):
        URL = URL[:-1]

    parsed = urlparse(URL)
    print(('\t' * depth) + parsed.geturl())
    visited.add(URL)

    r = requests.get(URL)
    soup = BeautifulSoup(r.text,'html.parser')

    nextLinks = []
    for link in soup.find_all('a'):
        #print(link.get('href'))
        temp = str(link.get('href'))
        if temp.startswith('#'):
            continue
        if temp.startswith('/'):
            temp = URL + temp
        if temp.endswith('/'):
            temp = temp[:-1]
        nextLinks.append(temp)
        #print(temp)

    for urls in nextLinks:
        #if str(urls) == "http://www.usu.edu/":
        #    print("yup")
        #print(urls)
        try:
            crawling(urls,depth+1,maxdepth,visited)
        except:
            depth -= 1
            continue

    return