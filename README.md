## README

This program visits every possible website within the tree of the given website. If a max depth is not specified, the default is set to 3. 

From the **command line**:

	python src/crawler.py {Absolute URL} {Max Depth = 3}

Errors will be produced if at least the Absolute URL is not present. If it is not an Absolute URL, an error will also be produced. 
Exceptions are in place to account for if a potential error will occur when recursing through a given website.

This program may go on for quite a while. If you wish to stop it, simply interupt the command using "^Z".
